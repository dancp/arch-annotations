# Jornada ao Arch

- [Instalação](https://gitlab.com/dancp/arch-annotations/blob/master/arch-install.md)
- [Pós Instalação](https://gitlab.com/dancp/arch-annotations/blob/master/arch-post-install.md)
- [Melhorias](https://gitlab.com/dancp/arch-annotations/blob/master/tweaks.md)